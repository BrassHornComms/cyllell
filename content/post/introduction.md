---
title: "Introduction to Cyllell"
date: "2018-10-31T02:00:00+00:00"
draft: false
tags:
 - introduction
 - im
 - vpn
 - tor
 - legislation
---
Welcome to the first edition of Cyllell.com, it's not quite a blog, not quite an ezine and not quite a news aggregator. It probably won't contain how-to articles or reviews but also won't just duplicate tweets / blog posts published elsewhere.

The Internet and the services that run on top of it is under attack from more
angles than ever before, the UK Government is demanding mass surveillance,
backdoors, state mandated censorship, age / identity verification and more. Google is tracking your offline purchases and Amazon et al are silently recording everything you do in your home. 

Thankfully there are plenty of organisations and people fighting to help keep
your Internet browsing secure, confidential and possible. Cyllell.com will endeavour to highlight the people, software and news that helps you protect yourself.

For those who don't speak welsh, cyllell is the word for knife and the Internet
is rapidly becoming something akin to a knife, a tool that is useful for many
things but at risk of dracanion legislation due to misuse by a few.

# Instant Messaging
## Cwtch
The people at [OpenPrivacy](https://openprivacy.ca) have made lots of interesting
progress in October. 

The server side of the application can now be easily spun up as docker container which makes deployment and rapid prototyping much easier. We'll hopefully see published images in docker hub soon.

On the client side we've seen interesting work on moving to version 3 onion services (no scraping of the HSDirs now!).

You can find the code [here](https://git.openprivacy.ca/cwtch.im/cwtch) and more about the project [here](https://cwtch.im)

## Signal
Despite some rocky news [regarding upgrades and backups](https://twitter.com/msuiche/status/1054158934313967616) signal announced an exciting new feature; [Sealed Sender](https://signal.org/blog/sealed-sender/).

In essence this helps remove some of the conversation meta-data that is visible to the Signal infrastructure. 

## Briar
The [Briar Project](https://briarproject.org) have also had an eventful month, they recently moved to [v3 Onions](https://twitter.com/BriarApp/status/1052913066407538688) and have [put a call for testers](https://twitter.com/BriarApp/status/1054416457415696384) to resolve the common complaint about requiring in person swapping of contact details.


# Tor
## Tor Project
The Tor project itself has had a stellar few months with the [launch of the 8.0](https://blog.torproject.org/new-release-tor-browser-80) version of the Tor Browser that includes new user onboarding, improved bridge fetching, better language support, support for .onion alt-svc headers _(which this blog leverages!)_ and the cool new SSL padlock style iconography.

There's also a replacement for OrFox in the works with the [Tor Android Browser](https://blog.torproject.org/new-alpha-release-tor-browser-android) _(it'll still need Orbot to run)_.

Finally the [2018 Fundraising drive](https://blog.torproject.org/strength-numbers-double-your-donation-mozillas-match) is being matched by Mozilla!

## Onion Routed 3G
[Brass Horn Communications](https://brasshorncommunications.uk) closed the beta
trial of the [Onion Routed 3G](https://or3g.space) project with Motherboard 
writing an interesting [review](https://motherboard.vice.com/en_us/article/d3qqj7/sim-card-forces-data-through-tor-brass-horn-communications).

## NosOignons
Nos Oignons have [launched](https://nos-oignons.net/campagne2018/index.en.html) their 2018 fund raising drive. Like TorServers.net and BrassHornCommunications.uk they are a non-profit entity that operates Tor Exit nodes.

# Legislation
## Digital Economy Act
The Government finally [published the Age Verification Guidance](https://www.gov.uk/government/publications/digital-economy-bill-part-3-online-pornography) that was needed after the implementation of the Age Verification blocking system was put back.

Jim Killock from the [Open Rights Group](https://openrightsgroup.org.uk) tweeted [a thread](https://twitter.com/jimkillock/status/1052160755590680578) about the impact.

At the end of the day, if you host your content with https://Ablative.Hosting there's not much the BBFC can do _(unless you explictly out yourself on your website)_.

## Investigatory Powers Act
The House of Lords recently debated the Tele2/Watson amendments to the Investigatory Powers Act in light of the success of the legal action against the Government.

The debate can be watched [here](https://www.parliamentlive.tv/Event/Index/33cee971-1009-4211-aca0-44f9af47413d).
